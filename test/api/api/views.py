"""
    Here goes all controllers function to render into page.
"""
from django.http import HttpResponse
from django.shortcuts import render
from datetime import datetime
"""
    This is the entry views to send some message to client.
    This is the entry test-learing point.
"""


def helloWorld(request):
    return HttpResponse("Hello world!")


"""
    This is the view to controll the login-page
"""


def loginPage(request):
    return HttpResponse("This is the login page")


""" This view allow us to control the home view """


def homePage(request):
    # include the year in the footerpage.
    year = datetime.now().year
    return render(request, "index.html", { 'year': year })


""" This is the register form fo sign-up users"""


def register(request, age):
    return HttpResponse(
        "This is the register form tin he view, we have a usern with age: " + str(age)
    )


""" This is the register pate to allows to user register in the app"""


def registerPage(request, tasks):
    return render(request, "index.html", {tasks})
