from django.db import models

# Create your models here \ aqui se crean todos los modelos nesarios para la aplicacion
# Cada modelo es una clase

class Comments(models.Model):
    # Este es un tipico ejemplo de modelamiento de datos
    name = models.CharField(max_length=255, null=False)
    score = models.IntegerField(default=3)
    comment = models.TextField(max_length=1000, null=True)

    def __str__(self):
        return self.name
    
# Aqui podemos definir otros modelos que necesitemos

# class Author(models.Mode):
    
