## 1. User experience
* Es como una persona, `el usuario` siente acerca de interactuar con el producto, or la expriencia de usar un producto.
* *La expriencia de usuario es la forma en que el usuario se siente cundao interactua o experimenta con el producto.*

## 2. Product
* Este es un `bien`, o `servicio`o una `function`.
* Puede ser cualquier cosa que genere algun tipo de valor agregado.

## 3. Caracteristicas
* Para que el `usuario` pueda tener un buena experiencia al momento de usar el producto este debe tener las siguientes caracteristicas.
### 3. 1. Usable
* El producto debe ser facil de usar por el usuario, obviando completamente la dificultad de uso.
* Para esto la funcionalidad, estructura del producto debe centrarse en el facil uso de este mismo.

### 3. 2. Equitativo
* El producto debe ser facil de usar y  de facil accesibilidad para estos.
* Debe ser accesible para personas con capacidades especiales o antecedentes
### 3. 3. Disfrutable
* El producto debe ser agradable de usar por las personas.
* Para ello los diseniadores de UX, deben tener en cuenta como se siente el usuario a la hora de usar el producto
* Fomenta la posibilidd de distrutar y generar felicidad o `confianza` al momento de usar el producto
### 3. 4. Util
* El producto debe ser util, es decir debe resolver `problemas` de lso usuarios.

## 4. Caracteristicas  de los UXs
    * Generalmente los diseniadores UX no saben dibujar o hacer cosas increhibles.
    * Son personas que tienen curiusidad y tienden a pensar por la gente y como estos piensan.
    * Se sentran en que el producto sea mas facil de usar y que la gente disfrute usando el producto.

1. Empaticos
* Es entender a las personas o a alguien cuando atravieza una situacion determinada.
2. Curiosos
3. Aprovechan las experiencias en los trabajos para crear productos utilies.

## 5. Tipos de UXs
### 5.1 Interaction designers
* Son personas enfocadas en el disenio de la experiencia de un producto y como este funciona
* Se centran en como conectar las necesidade de los usuarios con los objetivos comerciales, `con los que tienen disponibles`.

### 5.2 Visual designers
* Son personas enfocadas en el disenio o como el producto o la tecnologia que se esta desarrollando se ve o se `presenta` al usuario.
* Logotipos, ilustraciones, etc

### 5.3 Motion designers
* Son personas que se enfocan en pensar que se siente para un usuario moverse o `navegar` a través del producto.
* Crear transicion suaves que brinder una mejor sensacion al usuario

### 5.4 Graphic designers
* Son personas que se enfocan en la creacion los mensajes o la historia que se transmitira acerca del producto al usuario
* Son los encargados de enfocarse en como el `aspecto` `fisico` del producto que se entrega a los usuarios
* Invitacion, posters

## 5.6 UX designers
* Son las personas que se enfocan de pensar en como los usuarios `interactuan` con el producto.

### 5.6.1  UX researchers
* Son las personas que realizan la investigacion o estudios o entrevistas que ayudan a la empresa a aprender como las personas usan el producto.

### 5.6.2 UX writters
* Son los encargados de hacer que el texto que aparece en el producto 
sea mas claro para que el usuario tenga una experiencia mas intutitiva sobre el producto

### 5.6.3 Production Designers
* Son los encargados de asegurarse que el disenio inicial y el final coincidan con el producto `final esperado` y son los encargados de que todos los `assets` *(recursos)* esten listos para ser entregados al `engineering team`.
### 5.6.4 Assets
* Los todos los `recursos` com textos, imagenes que son las especificaciones del disenio como, fuentes, stilos, colores, tamanios y espaciado

### 5.6.5 UX engineers
* Son los especialistas que tranforman el intento del disenio en un experiencia funcional del producto para los usuarios.

### 5.6.6 UX programmers managers


## 6. Ciclo de vida de un producto
1. Producto development life cycle
- Es el proceso usado que consiste en transformar una idea en realidad.
- Existen 5 etapas en el ciclo de vida del producto
    1. `Brainstorm`: El equipo genera una lluvia de ideas para solucionar el problema
    2. `Define`: Se usan las lluvias de ideas halladas para enfocarse en le proceso.
    3. `Design`: Implementar todos los hallasgos previos del proceso en un nuevo disenio implementando varios herramientas
    4. `Test`: El equipo evalua el producto diseniado basado en los comentarios netamente de los usuarios.
    5. `Launch`: La parte final es publicar el producto a los usuarios.


