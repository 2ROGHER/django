## 1.Instalacion de Django
```bash
# Instalacion de Django en la terminal
$ python -m pip install Django==@version

# Creacion de un proyecto con Django. Este es como un plantilla para la construccion del projecto con Django.
$ django-admin startproject <project_name>
```
Este es una *framework, gratuito, echo en python, que cuenta con una versatilidad de herramientas, libreiras, agil y reutilizable*. Este nos permite realizar aplicaciones muchos mas potentes y *flexibles*, *escalbles*, gracias a sus librerias.

## Patron de Arquitectura MVC - MTV(Model Template View)
1. *Controlador*: Este es una funcion que contiene toda la logica de la aplicacion. Esta funcion se comunica con *modelos*.
2. *Modelos*: Esta es una *abtraccion* de los datos que tenemos enla base de datos. El controlador se comunica con el *modelo* de manera que mediante el *ORM* se puedan realizar *instanciaciones* de los datos en la *DB* y finalmente realizar las *interacciones* con el modelo y los datos de la base de datos que el controlador solcita.
3. *Vista*: Finalmete el *modelo* entrega los datos al *controlador* el cual realiza todas las operaciones y caluculos gracias a que el *modelo* se los provee. Finalmete, los datos calculados y listos son derivados a la *vista*, este finalmente los muestra en la *vista* al usuario final.

![MVC](image.png)

#### La arquitectura de MTV, de Django funciona de la mismas forma, pero el *View*  en este caso se le llama los *Controladores* y las *Templates* se llaman las *Vistas*

![MTV](image-1.png)

## Estructura de proyectos en Django
Django se divide en *aplicaciones* que se gestionan mediante modulos, y cada aplicaciones es un *directorio*.

Cada *aplicacion* se gestiona como un *paquete* o directorio.

Ejecucion del comando `migrate`. Este es un comando que se usa cada vez que se realiza alguna modificacion en la estructura de datos de la *base de datos*.

Esto es un comando que se usa para poder *persistir* los doatos o hacer un *backup* de los datos en la *base de datos*.

```bash
    <directory>$:  python manage.py migrate

    # Levantar el servidor
    $ python manage.py runserver
```
Las URLs apuntan a  las vistas y las *vistas* trabajan con los *modelos*.
Estos son diferentes funciones que se van  a asociar con las URLs, estas funciones son los que van a ejecutar la logica de la programacion enlazada a cada URL.

Los *Templates* no son ni mas ni menos que *archivos de texto* enpaquetados en un *HTML*, los cuales seran visibles a los usuarios.

Los *Templates* nos pemiten separar la *logica* de la parte visual de la aplicacion. Ademas nos permiten estructurar mejor el proyecto y modularizarlo mucho mas eficidentemente cada  uno de los proyectos.

## Archivos estaticos en Django y Python
Estos son los recursos a los que pueden acceder publicamente los usuarios de la app.

## Herencia de plantillas en Django
Esta herramienta nos permite *moduloarizar* y evitar la espaguetizacion de codigo largo, denso o extenso ,ademas la reutilizacion de codigo o de etiquetas duplicadas en el proyecto.

## Uso de <span style='color: red'>Includes</span>

## Modularazicion de componentes
La modularazion de componentes es una practica de programacion para poder reutilizar el codigo y hacerlo escalablea medida que crece.

Los modulos son funciones que nos permiten encapsular el codigo y prevenir la espagetizacion del codigo y hacer que cada modulo sea una pequenia aplicacion que **integramos** con la apliacion __pirncipal__ para que todo funcione <span style='color: #ff923d'>en conjunto</span>.

> Se puede pesnar la __modularizacion__ como piezad de lego que se usan para contruir una aplicacionmas compleja. Estos son tambien llamado templates.

> Esta es una practica recomendada a la hora de programar y realizar proyectos de alto nivel. 


![modularizacion](image-2.png)
### ventajas
- Construccion mas solida y escalable
- Agilidad en le proceso
- Reutilizacion de codigo
- Estructuracion de codigo
- Permite que el codigo sea mas limpio
- Permite encapsular el codigo y aislarlo del externo

Para crear una aplicacion de modularazacion debemos seguir los siguientes pasos, esto nos permite crear un proyecto que nos ofrece el manage.py file:

> Una aplicacion debe estar dentro de un <DIRECTORIO> para que de esta manera se pueda reutilizar los modulos en otros proyectos.

> Tenemos que decirle a Django que un app esta contenido dentro de un __proyecto__ tenemos que decirselo en el 'settings.py'.

```bash 
# Metodo para iniciar una aplicacion dentro de un modulo.
$ python manage.py startapp <appname>

# Para checkear que la aplicacion funcione  correctamente
$ python manage.py check <appname> # python manage.py check coments.
```

## Modelos  en Django
Aqui lo que haremos sera contruir nuestros modelos, que representan los modelos o los datos que el ORM gestinara eficientemente para la __transmision__ y __comunicacion__ de los datos en el proyecto.

Se crean clases para la __abtraccion__ de los modelos, que al ser __instanciados__ son creados como __registros__ de tablas en las bases de datos.

Las <span style='color:#fad239'>clases</span> son los respresetnaciones __abstraidos__ de los objetos que son cada uno de los registros de la base de datos, con estos objetos el ORM puede realizar las operaciones basicas de CRUD.

Los modelos son indistintos de las bases de datos, estos son __independientes__ de los motores de bases de datos que Django admite.

Cuando se crea un modelo dentro de "models.py" se crea una __migracion__ en la base de datos con el que estamos trabajando.

### Tipos de datos en Django
- ChartField()
- TextField()
- IntegerField()

> NOta: Cada vez que hagamos o realizemos una modificacion en el archivo de modelos tenemos que actualizar todos los modelos mediante el 'codigo' siguiente.

```bash 
01.chain

# solicitamos la migracion a DJango
$ python manage.py makemigrations
# Le pedimos a Django que realiza finalente la migraciion
$ python manage.py migrate
```

Ahora que pasa si queremos modificar los _registros_ o modelos de mi base de datos.

> Pues en este caso tenemos que realizar los siguientes pasos antes mencionados en 01.chain. asi cada vez que realizamos los cambios tenemos que seguir y ejecutar losmismos pasos.

### Nota ⚠️⚠️
- No debemos compremeter la __integridad de los datos__, esto quiere decir si el campo que tiene un tipo o una restriccion de campos definido y si queremos agregar o __poblar__ las tablas, Django nos avisara que cambiemos estos campos o la definicion en __el modelo__.

- Cada vez que realizamos la confirmacion o la migraciones se generan __firmas__ de estas versiones para que se puedan controllar mas eficientemente la carga de datos al sistema. Este se guarda el la tabla de __migraciones__ con cada firma correspondiente y mantieniedo siempre las versiones de estos mismos, de esa manera si nos repentimos de algunos cambios, podemos volver a dichos cambios genereados y guardarlos.
- Los modulos generalemte se coloca dentro de una carpeta de __(templates)__ todos aquellos modulos que seran usados en el proyecto.

### Funcionamiento de modelos en Django (CRUD)
Todod lo que hemos hecho hasta este momento solo esta habitando en la __aplicacion__, para poder ver el resultado tiene que levantar el servicio y hacer ajustes en los archivos que contienen los __URLs__ del proyecto y la __aplicaion__ en si.

Esto se conoce como la delegacion de los __URLs__ de cara a conseguir una mejor modularizacion del codigo, y que no solo la aplicacion gestione la vista, tambine todos sus URLs.

> Nota ⚠️⚠️⚠️:     
Cada modelo es una aplicacion, y como tal como es toda aplicacion debe gestionar sus propias VISTAS, sus propias URLs, y funcionalidade. ESto ayuda  a la modularizacion de codigo. !importantee!.

#### 1. Craete 📝
En este caso vamos a crearun nuevo comentario
#### 2. Delete 🗑️
Eliminar un objeto en el registro por _id_.
#### 3. Update 🖊️
Actualizar un el objeto por _id_.

### Estructura y claves foraneas en Modelos
Las  claves __foraneas__ son identificadores que nos permiten diferenciar cual es cual entre un conjunto de datos.

Las _Claves Foraneas_ nos permiten vincular o establecer las __relaciones__ entry cada una de las entidades o permite la comunicacion entre los objetos o la tabla a los contenidos d eotra tabla.

A la hora de realizar un "borrado" se puede llegar a los casos de __perdida de integridad__. Para mantener la integridad de los datos necesitamos verifcar que si un author es borrado sus entradas tambien deben ser borrados para  mantener la __integridad de datos__.

### Poblando la base de datos con Django-seed
Durante el desarrollo del back-end, no es <span style='color:#f2ad23'>poblar</span> los datos con con datos reales, sino tenemos que usar datos falsos para probar la aplicacion y realizar pruebas

- Se puede usar la aplicacion de: [django-seed](https://www.django-seed.org): Django-seed.
```bash 
$ pip  install django-seed
# para poblar la base de datos sera
$ python manage.py seed post --number=50
```

### Consulta de datos en Django
Metodos para realizar filtros con __cotas__.
![metodos para realizar filtros](image-3.png)

### Relaciones one-to-one, one-to-many, many-to-many
View project 06-relationship

## Conexion a otras bases de datos con Django
Para poder trabajar con otros sistemas de bases de datos, tenemos que configurar en los `settings.py` con ladocumentacion que esta el la pagina web de django.

## Modelamiento de las Bases de Datos
### a. Diagrama Conceptual:
Aqui se describen las propiedades que van a tener las entidades.
### b. Diagarama Logico: 
En este apartado se establecen las diferentes relaciones que se establecen entre las entidades. Se establecen las __claves primarias__ y las __claves foraneas__.
### c. Diagrama de Entidad Relacion
Ahora finalmente se establecen los tanto los nombres de los campos o propieades, las _claves_ primarias y foraneas y los __tipos de datos__ para cada una de los  propiedades.

Y tenemos que hacer las formas normales antes de entregar el final del proyecto.
### 1. Primera forma normal
### 2. Segunda forma normal
### 3. Tercera forma normal

> Nota: usaremos la herramienta  visual paradigm o se puede usar tambien diagram.io

## Formulario a traves de Clases 

## Panel de administracion de Django
- Este modulos nos permite realizar la gestion de Django de una aplicacion.

- Los ajusten se encuentran en el archivo __settings.py__.

```bash 
    # crear el superusario en django
    $ python manage.py createsuperuser
    >>> username: roger
    >>> password: (****)
    # Este nos permite crear el superusuario para acceder a la gestion de al aplicacion
```
_ Este nos permite delimitar __los acceso, permisos__ o __roles__ a los usuarios a ciertas partes de la aplicacion.
