from django.http import HttpResponse
from django.shortcuts import render
import os

def hello(req):
    return HttpResponse('hello!')

def bye(req):
    return HttpResponse('bye!, see you tomorrow!')

def adult(req, edad, txt):
    if edad >= 18:
        return HttpResponse('Eres mayor de edad'+ 'el texto es ', txt)
    else:
        return HttpResponse('No eres mayor de edad')

# Retorno de plantillas con Django
def template(req):
    # El contexto nos ayuda a agregar 'dinamicidad' a una url
    return render(req,'template.html', {}) # render(info_req,path(),'context') 

# view dinamico
def dinamyc(request, name):
    categories = ['code', 'design', 'marketing', 'testers']
    # En el contexto se puede pasar cualquier tipo de datos(objetos, tipos primitivos, clases, etc)
    # return render(request,'template_dinamico.html',{'name': name}) # main form
    return render(request,'template_dinamico.html',{'categorias': categories})

# rutas para recursos estaticos
def estaticos(request): 
    return render(request, 'estaticos.html', {})

# Herencia de plantillas en Django
def herencia(request):
    return render(request, 'herencia.thml', {})

def ejemplo(request):
    return render(request, 'ejemplo.html',{})

def otra(request):
    return render(request, 'otra.html', {})

# Esta es el handler del 'base.html' que es el entry point del proyecto
def base(request):
    return render(request,'base.html', {})


## index route
def index(request):
    return render(request,'index.html', {}),