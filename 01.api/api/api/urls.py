"""
URL configuration for api project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from . import views #This othe  way to import modules in python

# Ejecutar rutas con parametros en Django
urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello/', views.hello, name='hello'),
    path('bye/',views.bye, name='bye'),
    path('adult/<int:edad>/<str:txt>',views.adult ,name='post_number'),
    path('template/', views.template, name='template'),
    path('dinamyc/<str:name>', views.dinamyc, name='dinamyc'),
    path('estaticos/', views.estaticos,name='estaticos'),
    path('herencia/', views.herencia, name='herencia'),
    path('ejemplo/',views.ejemplo, name='ejemplo'),
    path('otra/',views.otra, name='otra'),
    path('base/', views.base, name='base'),
    path('', views.index, name='index'),
]
